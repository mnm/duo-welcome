package welcome

import (
	"net/http"

	"gitlab.com/mnm/duo-welcome/generated/view"
	"gitlab.com/mnm/duo-welcome/generated/web"

	"gitlab.com/mnm/duo/router"
)

// Handler returns the web handler for the welcome page
func Handler() http.Handler {
	router := router.New()
	views := view.New()
	return web.New(router, views)
}
