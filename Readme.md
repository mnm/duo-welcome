# Welcome Page

The welcome page when you first run the `duo` command.

**Note:** You should usually put the `generated/` folder in your `.gitignore`, but in this case we're using the web server as a library for the `duo` command, so we need to also include the generated code.
