package main

import (
	program "gitlab.com/mnm/duo-welcome/generated/program"
)

func main() {
	program.Start()
}
