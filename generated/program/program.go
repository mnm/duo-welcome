package program

import (
	fmt "fmt"
	view "gitlab.com/mnm/duo-welcome/generated/view"
	web "gitlab.com/mnm/duo-welcome/generated/web"
	router "gitlab.com/mnm/duo/router"
	http "net/http"
	os "os"
)

// Start the program
func Start() {
	p := &Program{os.Args[1:]}
	p.Start()
}

// Program struct
type Program struct {
	Args []string
}

// Start the program
func (p *Program) Start() {
  webWeb := Web()
  port := os.Getenv("PORT")
  if port == "" {
    port = "3000"
  }
  fmt.Println("Listening on http://localhost:"+port)
  if err := http.ListenAndServe(":"+port, webWeb); err != nil {
    fmt.Println("listen error", err)
    return
	}
}

// Web loads the web server
func Web() *web.Web {
	routerRouter := router.New()
	viewMap := view.New()
	webWeb := web.New(routerRouter, viewMap)
	return webWeb
}

