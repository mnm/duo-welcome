package view

import (
	response "gitlab.com/mnm/duo/response"
	router "gitlab.com/mnm/duo/router"
	view "gitlab.com/mnm/duo/view"
	gohtml "gitlab.com/mnm/duo/view/gohtml"
	fs "io/fs"
)

func New() Map {
	return Map{
    "index": gohtml.New("index.gohtml", `<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Welcome</title>
</head>
<body>
  <h2>Hello Duo!</h2>
</body>
</html>`),
  }
}

type Map map[string]view.View

var _ fs.FS = Map(nil)

func (m Map) Open(name string) (fs.File, error) {
	return nil, nil
}

// Mount provides default rendering for actionless views
// TODO: this should be done on build, not at runtime
func (m Map) Mount(router *router.Router) {
  router.Get(`/`, response.Render(m["index"], nil))
}

// Render the path
func (m Map) Render(path string, data interface{}) (string, error) {
	return "", nil
}
