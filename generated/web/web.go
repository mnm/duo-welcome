package web

import (
  view "gitlab.com/mnm/duo-welcome/generated/view"
  router "gitlab.com/mnm/duo/router"
)

// New web server
func New(
	router *router.Router,
	views view.Map,
) *Web {
  views.Mount(router)
  return &Web{router}
}

// Web server
type Web struct {
  *router.Router
}